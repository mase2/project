
Feature: Display all unique cause codes associated with IMSIs call failures
  As a Customer Service Rep. I want to see, for a given IMSI, 
  all the unique Cause Codes associated with its call failures, 
  so that I can review the most common causes of failure

Scenario: Display Cause Code for all failures for a given IMSI

Given: A valid IMSI is entered

When: all failures are selected

Then: A table with all the Cause Code for all call failures for a given IMSI is displayed to the user

 

Scenario: Invalid IMSI entered

Given: A invalid IMSI is entered

When: any combination of failures are selected

Then: An error will be thrown stating that an invalid IMSI has been entered

 

Scenario: No failures Recorded

Given: A valid IMSI is entered

When: any combination of failures are selected

And: the IMSI has no associated failures stored in the database

Then: The user will be prompted that there are no failures stored