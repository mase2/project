
Feature: Display event ID and cause code for failures affecting IMSIs
  As Customer Service Rep. I want to display, for a given affected IMSI, 
  the Event ID and Cause Code for any / all failures affecting that IMSI, 
  so that I can have customers identify the cause of their failure.

Scenario: Display Event ID and Cause Code for all failures for a given IMSI

Given: A valid IMSI is entered

When:  all failures are selected

Then: A table with all the Event ID and Cause Code for all failures for a given IMSI is displayed to the user

 

Scenario: Display Event ID and Cause Code for specific failures for a given IMSI

Given: A valid IMSI is entered

When:  specified failures are selected

Then: A table with all the Event ID and Cause Code for these failures for a given IMSI is displayed to the user

 

Scenario: Invalid IMSI entered

Given: An invalid IMSI is entered

When:  any combination of failures are selected

Then: An error will be thrown stating that an invalid IMSI has been entered 

 

Scenario: No failures Recorded

Given: A valid IMSI is entered

When:  any combination of failures are selected 

And: the IMSI has no associated failures stored in the database

Then: The user will be prompted that there are no failures stored