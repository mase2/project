
Feature: Receive and import datasets
  As the System Administrator I want to be able to receive and import Datasets so that the system has the data available to query and analyse

Scenario: Successful dataset import
Given: System Administrator is on data import panel
When: System Administrator imports dataset as csv file
Then: System Administrator creates database


Scenario: Unsuccessful dataset import
Given: System Administrator is on data import panel
When: System Administrator imports dataset as unsupported file type
Then: Error message is displayed
And: Database is not created
