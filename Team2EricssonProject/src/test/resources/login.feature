
Feature: Login Feature
  As a user, I want to login to the system so that I can view call data.

Scenario: Successful login
Given: User is on login page
When: User enters valid ID and password
And: User clicks login button
Then: User is taken to relevant dashboard


Scenario: Invalid password
Given: User is on login page
When: User enters invalid password
And: User clicks login button
Then: Invalid password warning appears


Scenario: Invalid ID
Given: User is on login page
When: User enters invalid ID
And: User clicks login button
Then: Invalid ID warning appears