
Feature: Assign ID and password to each user type
  As the System Administrator I want to be able to assign an ID and password to each User type (i.e. Customer Service Rep., Support Engineer, Network Management Engineer), so that these users can access the call data portal.
  
  
Scenario: Successfully assign ID and password to each user type
Given: System Admin is on their dashboard
When: System Admin can create a new user type in the database
Then: System Admin can assign an ID and password to each user type


Scenario: ID already exists
Given: System Admin is on their dashboard
When: System Admin assigns ID which is already on system
Then: Invalid ID warning appears


Scenario: Password too weak
Given: System Admin is on their dashboard
When: System Admin creates a password which is too weak
Then: Password strength message appears

