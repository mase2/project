
Feature: Count number of failures in given time period for IMSIs
  As a Customer Service Rep, I want to count, for a given IMSI, 
  the number of failures they have had during a given time period, 
  so that I can identify the scale of the problem.

Scenario: Display count of failures  for a given IMSI in a valid time range

Given: A valid IMSI is entered

When: valid specified time range is selected

Then: A count will be displayed for a given IMSI is displayed with the number of failures

 

Scenario: Display count of failures  for a given IMSI in an invalid time range

Given: A valid IMSI is entered

When: an invalid specified time range is selected

Then: an error message is displayed stating that an invalid time range is selected

 

Scenario: Invalid IMSI entered

Given: A invalid IMSI is entered

When: any combination of failures are selected

Then: An error will be thrown stating that an invalid IMSI has been entered