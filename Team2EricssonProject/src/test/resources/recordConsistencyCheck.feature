
Feature: Incoming records consistency check
As the System Administrator I want to have incoming records checked for consistency 
(i.e. valid date / time values, valid MNC / MCC combinations, valid Event IDs, Cause Codes etc.) 
and have erroneous records highlighted and excluded, so that only valid data is loaded into the system, 
a count of erroneous events is generated, and details of erroneous events are logged in the system.


Scenario: Erroneous Data Highlighted

Given: A user upload records to be stored in the database

When: Erroneous data such as invalid  date / time values, valid MNC / MCC combinations, valid Event IDs, Cause Codes etc are found

Then: These invalid data points are recorded 

And: the invalid data points are not stored in the database

 

Scenario: Erroneous Data Highlighted Count

Given: A user upload records to be stored in the database

When: Erroneous data such as invalid  date / time values, valid MNC / MCC combinations, valid Event IDs, Cause Codes etc are found

Then: These invalid data points are Counted and displayed to the user

And: this counter is stored in the database

And: the invalid data points are not stored in the database

 

Scenario: Store valid Records

Given: A user uploads records to be stored in the database

When: records pass the validation checks

Then: the file is stored in the database

And: no errors are logged